# Base conversion helper

### Usage:
```go
import . "gitlab.com/caub/baseconv"

// BaseConv(alphabet? string = '012...YZ-_')(input string, src_base? int = 10, dst_base? int = len(alphabet))
x := BaseConv("128", 10, 16) // "80"
```

### As Command:
```sh
go run cmd/baseconv.go -i 16 <<<67
> 1D
```
or
```sh
go build cmd/baseconv.go
./baseconv -i 16 <<<67
> 1D
```