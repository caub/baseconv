package baseconv

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBase_Conv(t *testing.T) {
	assert.Equal(t, BaseConv()("42"), "G", "should work with defaults")
	assert.Equal(t, BaseConv()("12345678987654321345678987654321"), "2rQMFeIiN45n18wDiN", "should work with defaults")

	assert.Equal(t, BaseConv()("42", 5), "m", "should work with different input base")
	assert.Equal(t, BaseConv()("42", 5, 20), "12", "should work with different output base")

	assert.Panics(t, func() {
		BaseConv()("42", 5, 80)
	}, "should error when the output base is higher than alphabet length")

	assert.Equal(t, BaseConv()("42", 4, 20), "2", "should skip characters that does't exist in alphabet")

	assert.Equal(t, BaseConv("0123456789#$@_!?")("4278977853"), "??0@053_", "should work with a different alphabet")
}
