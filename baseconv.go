package baseconv

import (
	"fmt"
	"strings"
)

// inspired from https://codegolf.stackexchange.com/questions/1620/arbitrary-base-conversion/21672#21672

// BaseConv convert string between different number bases/radius
func BaseConv(charsets ...string) func(string, ...int) string {
	charset := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"
	if len(charsets) > 0 {
		charset = charsets[0]
	}
	charsetMap := make(map[rune]int) // char => position map, to avoid a O(n) strings.IndexRune
	for i, r := range charset {
		charsetMap[r] = i
	}

	return func(s string, bases ...int) string {
		inputBase := 10
		ouputBase := len(charsetMap)
		if len(bases) > 0 {
			inputBase = bases[0]
			if len(bases) > 1 {
				ouputBase = bases[1]
			}
		}
		if ouputBase > len(charsetMap) {
			panic(fmt.Sprintf("dst radix exceeds current charset length (%v)", len(charsetMap)))
		}
		res := make([]int, 0)
		rs := []rune(s)
		nums := make([]int, len(rs))
		for i, v := range rs {
			nums[i] = charsetMap[v]
		}
		for len(nums) > 0 {
			// divide successive powers of ouputBase
			quotient := make([]int, 0)
			remainder := 0
			for i := 0; i < len(nums); i++ {
				if nums[i] >= inputBase {
					continue
				}
				accumulator := nums[i] + remainder*inputBase
				digit := accumulator / ouputBase // it's floored by default, numbers are integer by default
				remainder = accumulator % ouputBase
				if len(quotient) > 0 || digit > 0 {
					quotient = append(quotient, digit)
				}
			}
			// the remainder of current division is the next rightmost digit
			res = append(res, remainder)

			// rinse and repeat with next power of ouputBase
			nums = quotient
		}

		// prepare return value, reverse res, and map back digits (positions) to chars
		ret := make([]string, len(res))
		for i, v := range res {
			ret[len(ret)-1-i] = string(charset[v])
		}
		return strings.Join(ret, "")
	}
}
