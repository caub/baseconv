package main

import (
	"flag"
	"fmt"

	baseconv ".."
)

func main() {
	inputBasePtr := flag.Int("i", 10, "input base")
	outputBasePtr := flag.Int("o", 64, "output base")
	alphabetPtr := flag.String("a", "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_", "alphabet characters")
	flag.Parse()
	var input string
	fmt.Scanln(&input)
	fmt.Println(baseconv.BaseConv(*alphabetPtr)(input, *inputBasePtr, *outputBasePtr))
}
